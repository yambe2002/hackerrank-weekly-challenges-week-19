﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestWk()
        {
            var solver = new Solver();
            Assert.AreEqual(2, solver.Solve(4, 5, 3));
            Assert.AreEqual(0, solver.Solve(1000, 1000, 1));
            Assert.AreEqual(1, solver.Solve(1000, 1000, 2));
            Assert.AreEqual(0, solver.Solve(1000, 100000, 1));
            Assert.AreEqual(1, solver.Solve(1000, 100000, 2));
            Assert.AreEqual(97, solver.Solve(1, 333, 50));
            Assert.AreEqual(114, solver.Solve(1, 333, 300));
            Assert.AreEqual(114, solver.Solve(1, 333, 500));
            Assert.AreEqual(114, solver.Solve(1, 333, 5000));

            Assert.AreEqual(97, solver.Solve(1, 1000000000, 50));
            Assert.AreEqual(597, solver.Solve(1, 1000000000, 300));
            Assert.AreEqual(997, solver.Solve(1, 1000000000, 500));
            Assert.AreEqual(1997, solver.Solve(1, 1000000000, 1000));
            Assert.AreEqual(3997, solver.Solve(1, 1000000000, 2000));

            Assert.AreEqual(7, solver.Solve(3, 1000000000, 5));
            Assert.AreEqual(17, solver.Solve(3, 1000000000, 10));
            Assert.AreEqual(97, solver.Solve(3, 1000000000, 50));
            Assert.AreEqual(597, solver.Solve(3, 1000000000, 300));
            Assert.AreEqual(997, solver.Solve(3, 1000000000, 500));
            Assert.AreEqual(1997, solver.Solve(3, 1000000000, 1000));
            Assert.AreEqual(3997, solver.Solve(3, 1000000000, 2000));
            Assert.AreEqual(6, solver.Solve(1000, 10000000, 5));

            Assert.AreEqual(1, solver.Solve(1234, 2468, 5));
        }
    }

    public class Solver
    {
        public long Solve(long c, long m, long n)
        {
            c %= m;
            var resultSet = new HashSet<long>();

            var prev = c;
            long prev2 = 0;
            for (int i = 2; i <= 4 * n - 1; i++)
            {
                var val = (prev2 + prev) % m;

                if (i % 2 == 1 && i >= 7) resultSet.Add((val * c) % m);

                prev2 = prev;
                prev = val;
            }

            return resultSet.Count() % m;
        }
    }

    class Solution
    {
        static void Main(string[] args)
        {
            var solver = new Solver();
            var inputStr = Console.ReadLine();

            Console.WriteLine(solver.Solve(Int32.Parse(inputStr.Split(' ')[0]), Int32.Parse(inputStr.Split(' ')[1]), Int32.Parse(inputStr.Split(' ')[2])));
        }
    }
}
