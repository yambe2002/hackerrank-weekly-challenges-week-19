﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestWk()
        {
            var solver = new Solver();

            //sample 1
            solver.m = 5;
            solver.n = 4;
            solver.queries = new Tuple<int, int>[]{
                new Tuple<int, int>(1, 5),
                new Tuple<int, int>(3, 2),
                new Tuple<int, int>(4, 1),
                new Tuple<int, int>(2, 4),
            };
            Assert.AreEqual(11, solver.Solve());

            //sample 2
            solver.m = 4;
            solver.n = 2;
            solver.queries = new Tuple<int, int>[]{
                new Tuple<int, int>(1, 2),
                new Tuple<int, int>(4, 3),
            };
            Assert.AreEqual(2, solver.Solve());

            //sample 3
            solver.m = 10;
            solver.n = 3;
            solver.queries = new Tuple<int, int>[]{
                new Tuple<int, int>(2, 4),
                new Tuple<int, int>(5, 4),
                new Tuple<int, int>(9, 8),
            };
            Assert.AreEqual(5, solver.Solve());

            //minimum case
            solver.m = 2;
            solver.n = 1;
            solver.queries = new Tuple<int, int>[]{
                new Tuple<int, int>(2, 1),
            };
            Assert.AreEqual(1, solver.Solve());

            //2 -> 1
            //1 -> 2
            solver.m = 2;
            solver.n = 2;
            solver.queries = new Tuple<int, int>[]{
                new Tuple<int, int>(2, 1),
                new Tuple<int, int>(1, 2),
            };
            Assert.AreEqual(2, solver.Solve());

            //2 -> 1
            //1 -> 2 -> 1
            solver.m = 2;
            solver.n = 3;
            solver.queries = new Tuple<int, int>[]{
                new Tuple<int, int>(2, 1),
                new Tuple<int, int>(1, 2),
                new Tuple<int, int>(2, 1),
            };
            Assert.AreEqual(3, solver.Solve());

            //1 -> 2 -> 3
            //1 -> 2 -> 3
            solver.m = 3;
            solver.n = 4;
            solver.queries = new Tuple<int, int>[]{
                new Tuple<int, int>(1, 2),
                new Tuple<int, int>(1, 2),
                new Tuple<int, int>(2, 3),
                new Tuple<int, int>(2, 3),
            };
            Assert.AreEqual(4, solver.Solve());

            //1 -> 2 -> 3 -> 4 -> 5
            //1 -> 2 -> 3 -> 4 -> 5
            solver.m = 5;
            solver.n = 8;
            solver.queries = new Tuple<int, int>[]{
                new Tuple<int, int>(1, 2),
                new Tuple<int, int>(2, 3),
                new Tuple<int, int>(3, 4),
                new Tuple<int, int>(4, 5),
                new Tuple<int, int>(1, 2),
                new Tuple<int, int>(2, 3),
                new Tuple<int, int>(3, 4),
                new Tuple<int, int>(4, 5),
            };
            Assert.AreEqual(8, solver.Solve());

        }
    }

    public class Solver
    {
        public int n = 0;
        public int m = 0;
        public Tuple<int, int>[] queries;

        public int Solve()
        {
            int[][,] dp_all = new int[][,] { new int[m + 2, m + 2], new int[m + 2, m + 2] };

            int[] minIdx = new int[m + 1];
            for (int idx = 0; idx < m + 1; idx++)
            {
                minIdx[idx] = Int32.MaxValue;
            }

            for (int idx = 0; idx < queries.Length; idx++)
            {
                var val = queries[idx].Item2;
                if (minIdx[val] == Int32.MaxValue || idx < minIdx[val])
                {
                    minIdx[val] = idx;
                }
            }

            int dpIdx = 0;
            for (int idx = queries.Length - 1; idx >= 0; idx--)
            {
                var q = queries[idx];

                var pos1 = idx == 0 ? 0 : queries[idx-1].Item2;

                for (int pos2 = 0; pos2 < m + 1; pos2++)
                {
                    if (pos2 != 0 && idx < minIdx[pos2]) continue;  //cannot be

                    var dp = dp_all[dpIdx];
                    var dp_next = dp_all[dpIdx == 0 ? 1 : 0];

                    dp[Math.Min(pos1, pos2), Math.Max(pos1, pos2)] = Math.Abs(q.Item1 - q.Item2);

                    var movePos1 = (pos1 == 0 ? 0 : Math.Abs(pos1 - q.Item1))
                        + ((idx == queries.Length - 1) ? 0 : dp_next[Math.Min(q.Item2, pos2), Math.Max(q.Item2, pos2)]);

                    var movePos2 = (pos2 == 0 ? 0 : Math.Abs(pos2 - q.Item1))
                        + ((idx == queries.Length - 1) ? 0 : dp_next[Math.Min(q.Item2, pos1), Math.Max(q.Item2, pos1)]);

                    dp[Math.Min(pos1, pos2), Math.Max(pos1, pos2)] += Math.Min(movePos1, movePos2);
                }

                dpIdx = dpIdx == 0 ? 1 : 0;
            }

            return dp_all[dpIdx == 0 ? 1 : 0][0, 0];
        }
    }

    class Solution
    {
        static void Main(string[] args)
        {
            int T = 0;
            var inputStr = Console.ReadLine();
            T = Int32.Parse(inputStr);

            var outputs = new List<int>();

            for (int t = 0; t < T; t++)
            {
                var wk = new List<Tuple<int, int>>();
                var str = Console.ReadLine().Split(' ');
                var m = Int32.Parse(str[0]);
                var n = Int32.Parse(str[1]);

                for (int i = 0; i < n; i++)
                {
                    var str2 = Console.ReadLine().Split(' ');
                    wk.Add(new Tuple<int, int>(Int32.Parse(str2[0]), Int32.Parse(str2[1])));
                }

                var solve = new Solver();
                solve.n = n;
                solve.m = m;
                solve.queries = wk.ToArray();

                outputs.Add(solve.Solve());
            }

            foreach (var o in outputs)
                Console.WriteLine(string.Format("{0}", o));
        }
    }
}
