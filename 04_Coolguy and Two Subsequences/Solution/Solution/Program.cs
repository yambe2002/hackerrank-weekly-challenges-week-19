﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestWk()
        {
            var solver = new Solver();

            //sample case
            Assert.AreEqual((ulong)6, solver.Solve(3, new List<ulong>() { 3, 2, 1 }));

            Assert.AreEqual((ulong)6, solver.Solve(3, new List<ulong>() { 2, 3, 1 }));

            Assert.AreEqual((ulong)6, solver.Solve(3, new List<ulong>() { 1, 2, 3 }));

            Assert.AreEqual((ulong)20, solver.Solve(4, new List<ulong>() { 4, 1, 2, 3 }));

            Assert.AreEqual((ulong)92, solver.Solve(5, new List<ulong>() { 3, 2, 5, 6, 9 }));

            Assert.AreEqual((ulong)96, solver.Solve(5, new List<ulong>() { 2, 3, 5, 6, 9 }));

            Assert.AreEqual((ulong)373, solver.Solve(7, new List<ulong>() { 3, 2, 5, 6, 9, 5, 5 }));

            Assert.AreEqual((ulong)393, solver.Solve(7, new List<ulong>() { 2, 3, 5, 5, 5, 6, 9 }));

            Assert.AreEqual((ulong)1835, solver.Solve(10, new List<ulong>() { 2, 3, 5, 5, 5, 6, 9, 11, 16, 19 }));

            Assert.AreEqual((ulong)1523, solver.Solve(10, new List<ulong>() { 19, 11, 16, 2, 3, 5, 5, 5, 6, 9, }));

            Assert.AreEqual((ulong)2, solver.Solve(2, new List<ulong>() { 3, 2 }));

            Assert.AreEqual((ulong)999999999, solver.Solve(2, new List<ulong>() { 1000000000, 999999999 }));

            Assert.AreEqual((ulong)0, solver.Solve(1, new List<ulong>() { 1000000000 }));

            Assert.AreEqual((ulong)600000000, solver.Solve(3, new List<ulong>() { 300000000, 200000000, 100000000 }));
        }
    }

    public class Solver
    {
        const ulong MOD = 1000000007;
        ulong SillySolve(ulong n, ulong[] arr)
        {
            ulong ret = 0;

            System.IO.File.Delete("log.txt");

            for (ulong a = 0; a < n; a++)
            {
                for (ulong b = a; b < n; b++)
                {
                    var ab = f(arr, a, b);

                    for (ulong c = b + 1; c < n; c++)
                    {
                        for (ulong d = c; d < n; d++)
                        {
                            ret += Math.Min(ab, f(arr, c, d));
                            ret %= MOD;

                            System.IO.File.AppendAllText("log.txt", string.Format("({0}, {1}), ({2}, {3})\r\n", a, b, c, d));
                        }
                    }
                }
            }

            
            return ret;
        }

        ulong f(ulong[] arr, ulong a, ulong b)
        {
            var ret = ulong.MaxValue;

            while (a <= b)
            {
                ret = Math.Min(arr[a], ret);
                a++;
            }
            return ret;
        }

        public ulong Solve(ulong n, List<ulong> a)
        {
            var arr = a.ToArray();
           return SillySolve(n, arr);
        }
    }

    class Solution
    {
        static void Main(string[] args)
        {
            var solver = new Solver();

            ulong n = ulong.Parse(Console.ReadLine());
            var a = new List<ulong>();
            var arStr  = Console.ReadLine().Split(' ');

            for (ulong i = 0; i < n; i++)
            {
                a.Add(ulong.Parse(arStr[i]));
            }

            Console.WriteLine(solver.Solve(n, a));
        }
    }
}
