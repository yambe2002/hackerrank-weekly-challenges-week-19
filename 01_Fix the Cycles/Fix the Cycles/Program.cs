﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fix_the_Cycles
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestWk()
        {
            Assert.AreEqual(2, Program.Solve(2, -5, 0, 1, 1, 1));   //sample case
            Assert.AreEqual(0, Program.Solve(0, 0, 0, 0, 0, 0));
            Assert.AreEqual(1, Program.Solve(0, 0, -1, 0, 0, -1));
            Assert.AreEqual(80, Program.Solve(-20, -20, -20, -20, -20, -20));
            Assert.AreEqual(80, Program.Solve(-20, -20, -20, -20, 20, 20));
        }
    }

    class Program
    {
        static bool Success(int a, int b, int c, int d, int e, int f, int val, int idx)
        {
            switch (idx)
            {
                case 0: a += val; break;
                case 1: b += val; break;
                case 2: c += val; break;
                case 3: d += val; break;
                case 4: e += val; break;
                case 5: f += val; break;
            }

            return (b + f + a >= 0) && (b + c + d + a >= 0) && (e + d + a >= 0);
        }

        public static int Solve(int a, int b, int c, int d, int e, int f)
        {
            for (int i = 0; i <= 80; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (Success(a, b, c, d, e, f, i, j)) return i;
                }
            }

            return -1;  //should not be here since all loops have edge 'a'
        }

        static int Solve(string inputStr)
        {
            int a, b, c, d, e, f;
            a = Int32.Parse(inputStr.Split()[0]);
            b = Int32.Parse(inputStr.Split()[1]);
            c = Int32.Parse(inputStr.Split()[2]);
            d = Int32.Parse(inputStr.Split()[3]);
            e = Int32.Parse(inputStr.Split()[4]);
            f = Int32.Parse(inputStr.Split()[5]);
            return Solve(a, b, c, d, e, f);
        }

        static void Main(string[] args)
        {
            var inputStr = Console.ReadLine();
            Console.WriteLine(Solve(inputStr));
        }
    }
}
