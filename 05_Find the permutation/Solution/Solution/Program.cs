﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solution
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestWk()
        {
            var solver = new Solver();

            //sample case
            //Assert.AreEqual("3 1 2", solver.Solve(3, 5));
            Assert.AreEqual("3 1 4 2", solver.Solve(4, 2));
            Assert.AreEqual("-1", solver.Solve(4, 3));
        }

    }

    public static class SomeExtensions
    {
        public static IEnumerable<IEnumerable<T>> GetPermutations<T>(this IEnumerable<T> enumerable)
        {
            var array = enumerable as T[] ?? enumerable.ToArray();

            var factorials = Enumerable.Range(0, array.Length + 1)
                .Select(Factorial)
                .ToArray();

            for (var i = 0L; i < factorials[array.Length]; i++)
            {
                var sequence = GenerateSequence(i, array.Length - 1, factorials);

                yield return GeneratePermutation(array, sequence);
            }
        }

        private static IEnumerable<T> GeneratePermutation<T>(T[] array, IReadOnlyList<int> sequence)
        {
            var clone = (T[])array.Clone();

            for (int i = 0; i < clone.Length - 1; i++)
            {
                Swap(ref clone[i], ref clone[i + sequence[i]]);
            }

            return clone;
        }

        private static int[] GenerateSequence(long number, int size, IReadOnlyList<long> factorials)
        {
            var sequence = new int[size];

            for (var j = 0; j < sequence.Length; j++)
            {
                var facto = factorials[sequence.Length - j];

                sequence[j] = (int)(number / facto);
                number = (int)(number % facto);
            }

            return sequence;
        }

        static void Swap<T>(ref T a, ref T b)
        {
            T temp = a;
            a = b;
            b = temp;
        }

        private static long Factorial(int n)
        {
            long result = n;

            for (int i = 1; i < n; i++)
            {
                result = result * i;
            }

            return result;
        }
    }

    public class Solver
    {
        public string SillySolve(int n, int k)
        {
            var perm = new List<int[]>();
            var arry = new List<int>();

            for (int i = 1; i < n + 1; i++) arry.Add(i);
            foreach (var p in arry.ToArray().GetPermutations())
            {
                perm.Add(p.ToArray());
            }

            //find the max distance
            var distToIdx = new Dictionary<int, List<int>>();
            var maxDist = Int32.MinValue;
            for (int idx = 0; idx < perm.Count(); idx++ )
            {
                var p = perm[idx];
                var minDist = Int32.MaxValue;
                for (int i = 0; i < p.Length - 1; i++)
                {
                    minDist = Math.Min(minDist, Math.Abs(p[i] - p[i + 1]));
                }

                if (distToIdx.ContainsKey(minDist)) distToIdx[minDist].Add(idx);
                else distToIdx.Add(minDist, new List<int>() { idx });

                maxDist = Math.Max(minDist, maxDist);
            }

            //indices with the max distance array
            var targetIndices = distToIdx[maxDist];
            if (targetIndices.Count() < k) return "-1";

            return perm[targetIndices[k - 1]].Select(v => string.Format("{0}", v)).Aggregate((a, b) => a + " " + b);
        }

        public string Solve(int n, int k)
        {
            return SillySolve(n, k);
        }
    }

    class Solution
    {
        static void Main(string[] args)
        {
            var solver = new Solver();

            var t = Int32.Parse(Console.ReadLine());
            for (int i = 0; i < t; i++)
            {
                var inStr = Console.ReadLine();
                var n = Int32.Parse(inStr.Split(' ')[0]);
                var k = Int32.Parse(inStr.Split(' ')[1]);

                Console.WriteLine(solver.Solve(n, k));
            }
        }
    }
}
